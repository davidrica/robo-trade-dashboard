import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Checkbox,
  FormHelperText,
  TextField,
  Typography,
  Link
} from '@material-ui/core';
import useAuth from '../../../hooks/useAuth';
import useIsMountedRef from '../../../hooks/useIsMountedRef';

const RegisterJWT = (props) => {
  const isMountedRef = useIsMountedRef();
  const { register } = useAuth();
  const phoneRegExp = /\(?(?:\+|62|0)(?:\d{2,3})?\)?[ .-]?\d{2,4}[ .-]?\d{2,4}[ .-]?\d{2,4}/;

  return (
    <Formik
      initialValues={{
        first_name: '',
        last_name: '',
        email: '',
        phone_number: '',
        password: '',
        policy: false,
        submit: null
      }}
      validationSchema={Yup
        .object()
        .shape({
          first_name: Yup
            .string()
            .max(30)
            .required('First Name is required'),
          last_name: Yup
            .string()
            .max(255)
            .required('Last Name is required'),
          email: Yup
            .string()
            .email('Must be a valid Email')
            .max(255)
            .required('Email is required'),
          phone_number: Yup
            .string().matches(phoneRegExp, 'Format nomor handphone salah')
            .max(16)
            .required('Phone Number is required'),
          password: Yup
            .string()
            .min(7)
            .max(255)
            .required('Password is required'),
          policy: Yup
            .boolean()
            .oneOf([true], 'This field must be checked')
        })}
      onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        try {
          await register(values.first_name, values.last_name, values.email, values.phone_number, values.password);

          if (isMountedRef.current) {
            setStatus({ success: true });
            setSubmitting(false);
          }
        } catch (err) {
          console.error(err);
          setStatus({ success: false });
          setErrors({ submit: err.message });
          setSubmitting(false);
        }
      }}
    >
      {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
        <form
          noValidate
          onSubmit={handleSubmit}
          {...props}
        >
          <TextField
            error={Boolean(touched.first_name && errors.first_name)}
            fullWidth
            helperText={touched.first_name && errors.first_name}
            label="First Name"
            margin="normal"
            name="first_name"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.first_name}
            variant="outlined"
          />
          <TextField
            error={Boolean(touched.last_name && errors.last_name)}
            fullWidth
            helperText={touched.last_name && errors.last_name}
            label="Last Name"
            margin="normal"
            name="last_name"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.last_name}
            variant="outlined"
          />
          <TextField
            error={Boolean(touched.email && errors.email)}
            fullWidth
            helperText={touched.email && errors.email}
            label="Email Address"
            margin="normal"
            name="email"
            onBlur={handleBlur}
            onChange={handleChange}
            type="email"
            value={values.email}
            variant="outlined"
          />
          <TextField
            error={Boolean(touched.phone_number && errors.phone_number)}
            fullWidth
            helperText={touched.phone_number && errors.phone_number}
            label="Phone Number"
            margin="normal"
            name="phone_number"
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.phone_number}
            variant="outlined"
          />
          <TextField
            error={Boolean(touched.password && errors.password)}
            fullWidth
            helperText={touched.password && errors.password}
            label="Password"
            margin="normal"
            name="password"
            onBlur={handleBlur}
            onChange={handleChange}
            type="password"
            value={values.password}
            variant="outlined"
          />
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              ml: -1,
              mt: 2
            }}
          >
            <Checkbox
              checked={values.policy}
              color="primary"
              name="policy"
              onChange={handleChange}
            />
            <Typography
              color="textSecondary"
              variant="body2"
            >
              I have read the
              {' '}
              <Link
                color="primary"
                component="a"
                href="#"
              >
                Terms and Conditions
              </Link>
            </Typography>
          </Box>
          {Boolean(touched.policy && errors.policy) && (
            <FormHelperText error>
              {errors.policy}
            </FormHelperText>
          )}
          {errors.submit && (
            <Box sx={{ mt: 3 }}>
              <FormHelperText error>
                {errors.submit}
              </FormHelperText>
            </Box>
          )}
          <Box sx={{ mt: 2 }}>
            <Button
              color="primary"
              disabled={isSubmitting}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              Register
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
};

export default RegisterJWT;
