import { Avatar, Box, Card, Grid, LinearProgress, Typography } from '@material-ui/core';
import Label from '../../Label';
import CurrencyDollarIcon from '../../../icons/CurrencyDollar';
import CashIcon from '../../../icons/Cash';
import CalendarIcon from '../../../icons/Calendar';

const QuickStats2RT = () => (
  <Box
    sx={{
      backgroundColor: 'background.default'
    }}
  >
    <Grid
      container
      spacing={2}
    >
      <Grid
        item
        xs={12}
        md={6}
        lg={3}
      >
        <Card
          sx={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'space-between',
            p: 3
          }}
        >
          <Box sx={{ flexGrow: 1 }}>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="overline"
            >
              Balance
            </Typography>
            <Box
              sx={{
                alignItems: 'center',
                display: 'flex',
                flexWrap: 'wrap'
              }}
            >
              <Typography
                color="textPrimary"
                sx={{ mr: 1 }}
                variant="h5"
              >
                $24,000
              </Typography>
              <Label color="success">
                4%
              </Label>
            </Box>
          </Box>
          <Avatar
            sx={{
              backgroundColor: 'error.main',
              color: 'primary.contrastText',
              height: 48,
              width: 48
            }}
          >
            <CashIcon fontSize="small" />
          </Avatar>
        </Card>
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        lg={3}
      >
        <Card
          sx={{
            alignItems: 'center',
            // backgroundColor: 'primary.main',
            // color: 'primary.contrastText',
            display: 'flex',
            justifyContent: 'space-between',
            p: 3
          }}
        >
          <Box sx={{ flexGrow: 1 }}>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="overline"
            >
              Total Profit
            </Typography>
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                flexWrap: 'wrap'
              }}
            >
              <Typography
                color="inherit"
                variant="h5"
              >
                $25.50
              </Typography>
            </Box>
          </Box>
          <Avatar
            sx={{
              backgroundColor: 'success.main',
              color: 'primary.contrastText',
              height: 48,
              width: 48
            }}
          >
            <CurrencyDollarIcon fontSize="small" />
          </Avatar>
        </Card>
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        lg={3}
      >
        <Card
          sx={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'space-between',
            p: 3
          }}
        >
          <Box sx={{ flexGrow: 1 }}>
            <Typography
              color="textSecondary"
              gutterBottom
              variant="overline"
            >
              Total Days
            </Typography>
            <Box
              sx={{
                alignItems: 'center',
                display: 'flex',
                flexWrap: 'wrap'
              }}
            >
              <Typography
                color="textPrimary"
                sx={{ mr: 1 }}
                variant="h5"
              >
                23
              </Typography>
            </Box>
          </Box>
          <Avatar
            sx={{
              backgroundColor: 'warning.main',
              color: 'primary.contrastText',
              height: 48,
              width: 48
            }}
          >
            <CalendarIcon fontSize="small" />
          </Avatar>
        </Card>
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        lg={3}
      >
        <Card sx={{ p: 3 }}>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="overline"
          >
            Next Payout
          </Typography>
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              flexWrap: 'wrap'
            }}
          >
            <Typography
              color="textPrimary"
              sx={{ mr: 1 }}
              variant="h5"
            >
              7 days
            </Typography>
            <Box sx={{ flexGrow: 1 }}>
              <LinearProgress
                color="primary"
                value={74}
                variant="determinate"
              />
            </Box>
          </Box>
        </Card>
      </Grid>
    </Grid>
  </Box>
);

export default QuickStats2RT;
