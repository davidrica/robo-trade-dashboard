import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  LinearProgress,
  Typography
} from '@material-ui/core';

const QuickStats7RT = () => (
  <Box
    sx={{
      backgroundColor: 'background.default',
      minHeight: '100%',
      p: 3
    }}
  >
    <Card>
      <CardHeader title="Next Payout" />
      <Divider />
      <CardContent>
        <LinearProgress
          value={50}
          variant="determinate"
        />
        <Box sx={{ mt: 2 }}>
          <Typography
            color="textSecondary"
            variant="subtitle2"
          >
            14 days to payout
          </Typography>
        </Box>
      </CardContent>
    </Card>
  </Box>
);

export default QuickStats7RT;
