import numeral from 'numeral';
import {
  Badge,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  List,
  ListItem,
  ListItemText,
  Typography,
  LinearProgress
} from '@material-ui/core';
import PropTypes from 'prop-types';
import ArrowRightIcon from '../../../icons/ArrowRight';

const currencies = [
  {
    amount: 21500,
    color: '#6C76C4',
    name: 'US Dollars'
  },
  {
    amount: 15300,
    color: '#33BB78',
    name: 'Bitcoin'
  },
  {
    amount: 1076.81,
    color: '#FF4081',
    name: 'XRP Ripple'
  }
];

function LinearProgressWithLabel(props) {
  return (
    <Box display="block" alignItems="center" sx={{ pb: 4 }}>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">
          {`${Math.round(props.value)} days to payout`}
        </Typography>
      </Box>
      <Box width="100%" mr={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
    </Box>
  );
}

LinearProgressWithLabel.propTypes = {
  /**
   * The value of the progress indicator for the determinate and buffer variants.
   * Value between 0 and 100.
   */
  value: PropTypes.number.isRequired
};

const OverviewTotalBalance = (props) => (
  <Card {...props}>
    <CardHeader
      subheader={(
        <Typography
          color="textPrimary"
          variant="h4"
        >
          {numeral(2200).format('$0,0.00')}
        </Typography>
      )}
      sx={{ pb: 0 }}
      title={(
        <Typography
          color="textSecondary"
          variant="overline"
        >
          Total balance
        </Typography>
      )}
    />
    <CardContent>
      <Divider sx={{ mb: 2 }} />
      <Typography
        color="textSecondary"
        variant="overline"
      >
        Next Profit
      </Typography>
      <LinearProgressWithLabel variant="determinate" value={50} />
      <Divider />
      <Box
        sx={{
          alignItems: 'flex-start',
          display: 'flex',
          flexDirection: 'column',
          pt: 2
        }}
      >
        <Button
          color="primary"
          endIcon={<ArrowRightIcon fontSize="small" />}
          variant="text"
        >
          Add money
        </Button>
        <Button
          color="primary"
          endIcon={<ArrowRightIcon fontSize="small" />}
          variant="text"
        >
          Withdraw funds
        </Button>
      </Box>
    </CardContent>
  </Card>
);

export default OverviewTotalBalance;
