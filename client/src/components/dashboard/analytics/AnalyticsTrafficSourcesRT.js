import { useState } from 'react';
import Chart from 'react-apexcharts';
import { Box, Card, CardHeader, Checkbox, Tooltip, Typography, LinearProgress } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import InformationCircleIcon from '../../../icons/InformationCircle';

const profitList = [];
const balanceList = [];
const dateList = [];
let dayProfit = 0;
let dayBalance = 2000;
const monthlyProfit = 200;
for (let i = 0; i < 30; i++) {
  dayProfit += (monthlyProfit / 30);
  // dayProfit = (monthlyProfit / 30);
  dayBalance += (monthlyProfit / 30);
  profitList.push(dayProfit.toFixed(2));
  balanceList.push(dayBalance.toFixed(2));
  dateList.push(`${i + 1} May`);
}

const data = {
  series: [
    {
      color: '#4CAF50',
      data: profitList,
      name: 'Profit'
    },
    {
      color: '#FF9800',
      data: balanceList,
      // data: [35, 41, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35],
      name: 'Balance'
    }
    // {
    //   color: '#F44336',
    //   data: [100, 122, 50, 300, 250, 400, 312, 200, 10, 60, 90, 400],
    //   name: 'Social Media'
    // }
  ],
  xaxis: {
    dataPoints: dateList
    // dataPoints: [
    //   '01 Jan',
    //   '02 Jan',
    //   '03 Jan',
    //   '04 Jan',
    //   '05 Jan',
    //   '06 Jan',
    //   '07 Jan',
    //   '08 Jan',
    //   '09 Jan',
    //   '10 Jan',
    //   '11 Jan',
    //   '12 Jan'
    // ]
  }
};

const AnalyticsTrafficSourcesRT = (props) => {
  const theme = useTheme();
  const [selectedSeries, setSelectedSeries] = useState([
    'Profit',
    'Balance'
    // 'Social Media'
  ]);

  const handleChange = (event, name) => {
    if (!event.target.checked) {
      setSelectedSeries(selectedSeries.filter((item) => item !== name));
    } else {
      setSelectedSeries([...selectedSeries, name]);
    }
  };

  const visibleSeries = data.series.filter((item) => selectedSeries.includes(item.name));

  const chart0 = {
    options: {
      chart: {
        background: 'transparent',
        stacked: false,
        toolbar: {
          show: false
        },
        zoom: false
      },
      colors: visibleSeries.map((item) => item.color),
      dataLabels: {
        enabled: false
      },
      grid: {
        borderColor: theme.palette.divider,
        xaxis: {
          lines: {
            show: true
          }
        },
        yaxis: {
          lines: {
            show: true
          }
        }
      },
      legend: {
        show: false
      },
      markers: {
        hover: {
          size: undefined,
          sizeOffset: 2
        },
        radius: 2,
        shape: 'circle',
        size: 4,
        strokeWidth: 0
      },
      stroke: {
        curve: 'smooth',
        lineCap: 'butt',
        width: 3
      },
      theme: {
        mode: theme.palette.mode
      },
      tooltip: {
        theme: theme.palette.mode
      },
      xaxis: {
        axisBorder: {
          color: theme.palette.divider
        },
        axisTicks: {
          color: theme.palette.divider,
          show: true
        },
        categories: data.xaxis.dataPoints,
        labels: {
          style: {
            colors: theme.palette.text.secondary
          }
        }
      },
      yaxis: [
        {
          axisBorder: {
            color: theme.palette.divider,
            show: true
          },
          axisTicks: {
            color: theme.palette.divider,
            show: true
          },
          labels: {
            style: {
              colors: theme.palette.text.secondary
            }
          }
        },
        {
          axisTicks: {
            color: theme.palette.divider,
            show: true
          },
          axisBorder: {
            color: theme.palette.divider,
            show: true
          },
          labels: {
            style: {
              colors: theme.palette.text.secondary
            }
          },
          opposite: true
        }
      ]
    },
    series: visibleSeries
  };

  const chart = {
    series: [{
      name: 'Profit',
      type: 'column',
      // data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3.8, 4.6]
      data: profitList
    }, 
    // {
    //   name: 'Cashflow',
    //   type: 'column',
    //   data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6.5, 8.5]
    // }, 
    {
      name: 'Balance',
      type: 'column',
      // data: [20, 29, 37, 36, 44, 45, 50, 58]
      data: balanceList
    }],
    options: {
      chart: {
        height: 350,
        type: 'column',
        stacked: false
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        width: [1, 1, 4]
      },
      title: {
        text: 'Profit - Balance Analysis (Last 30 days)',
        align: 'left',
        offsetX: 110
      },
      xaxis: {
        // categories: [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016],
        categories: dateList
      },
      yaxis: [
        {
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: '#008FFB'
          },
          labels: {
            style: {
              colors: '#008FFB',
            }
          },
          title: {
            text: 'Profit (USD)',
            style: {
              color: '#008FFB',
            }
          },
          tooltip: {
            enabled: true
          }
        },
        // {
        //   seriesName: 'Profit',
        //   opposite: true,
        //   axisTicks: {
        //     show: true,
        //   },
        //   axisBorder: {
        //     show: true,
        //     color: '#00E396'
        //   },
        //   labels: {
        //     style: {
        //       colors: '#00E396',
        //     }
        //   },
        //   title: {
        //     text: 'Operating Cashflow (USD)',
        //     style: {
        //       color: '#00E396',
        //     }
        //   },
        // },
        {
          seriesName: 'Balance',
          opposite: true,
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: 'rgb(0, 227, 150)'
          },
          labels: {
            style: {
              colors: 'rgb(0, 227, 150)',
            },
          },
          title: {
            text: 'Balance (USD)',
            style: {
              color: 'rgb(0, 227, 150)',
            }
          }
        },
      ],
      tooltip: {
        fixed: {
          enabled: true,
          position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
          offsetY: 30,
          offsetX: 60
        },
      },
      legend: {
        horizontalAlign: 'left',
        offsetX: 40
      }
    }
  };

  return (
    <Card {...props}>
      <CardHeader
        disableTypography
        title={(
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <Typography
              color="textPrimary"
              variant="h6"
            >
              Profit - Balance Analysis
            </Typography>
            {/* <Tooltip title="Widget25 Source by channel">
              <InformationCircleIcon fontSize="small" />
            </Tooltip> */}
          </Box>
        )}
      />
      {/* <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexWrap: 'wrap',
          px: 2
        }}
      >
        {data.series.map((item) => (
          <Box
            key={item.name}
            sx={{
              alignItems: 'center',
              display: 'flex',
              mr: 2
            }}
          >
            <Checkbox
              checked={selectedSeries.some((visibleItem) => visibleItem === item.name)}
              color="primary"
              onChange={(event) => handleChange(event, item.name)}
            />
            <Box
              sx={{
                backgroundColor: item.color,
                borderRadius: '50%',
                height: 8,
                ml: 1,
                mr: 2,
                width: 8
              }}
            />
            <Typography
              color="textPrimary"
              variant="subtitle2"
            >
              {item.name}
            </Typography>
          </Box>
        ))}
      </Box> */}
      <Chart
        height="393"
        type="line"
        {...chart}
      />
    </Card>
  );
};

export default AnalyticsTrafficSourcesRT;
