const idTypes = [
  { text: 'National ID', value: 'national_id' },
  { text: 'Passport', value: 'passport' },
  { text: 'Driver Licence', value: 'driver_licence' }
];

export default idTypes;
