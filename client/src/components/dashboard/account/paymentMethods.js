const paymentMethods = [
  { text: 'Bank Transfer', value: 'bank_transfer' },
  { text: 'Wallet Transfer (USDT)', value: 'wallet_Transfer' }
];

export default paymentMethods;
