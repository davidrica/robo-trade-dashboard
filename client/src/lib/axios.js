import axios from 'axios';
// import AxiosMockAdapter from 'axios-mock-adapter';

// const axiosInstance = axios.create();
const axiosInstance = axios;

// const axiosInstance = axios.create({
//   baseURL: 'http://localhost:3001'
// });

axiosInstance.interceptors.response.use((response) => response,
  (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong'));

// export const mock = new AxiosMockAdapter(axiosInstance, { delayResponse: 0 });
export const mock = axiosInstance;

export default axiosInstance;
