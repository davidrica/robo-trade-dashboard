const express = require("express");
const app = express();
var cors = require('cors');
const db = require("./models");
const { users } = require("./models");

const bcrypt = require("bcrypt");
const cookieParser = require("cookie-parser");
const { createTokens, validateToken } = require("./JWT");

app.use(express.json());
app.use(cookieParser());
app.use(cors());

app.post("/api/authentication/register", (req, res) => {
  const { email, password, first_name, last_name, phone_number } = req.body;
  bcrypt.hash(password, 10).then((hash) => {
    console.log('first_name: ', first_name);
    console.log('last_name: ', last_name);
    console.log('email: ', email);
    console.log('phone_number: ', phone_number);
    console.log('password: ', password);
    console.log('passwordHash: ', hash);
    users.create({
      first_name: first_name,
      last_name: last_name,
      email: email,
      phone_number: phone_number,
      password: hash
    })
      .then(() => {
        res.json("USER REGISTERED");
      })
      .catch((err) => {
        if (err) {
          res.status(400).json({ error: err });
        }
      });
  });
});

app.post("/api/authentication/login", async (req, res) => {
  const { email, password } = req.body;
  console.log('email: ', email);
  console.log('password: ', password);

  const user = await users.findOne({ where: { email: email } });

  if (!user) res.status(400).json({ message: "User doesn't exist" });

  const dbPassword = user.password;
  bcrypt.compare(password, dbPassword).then((match) => {
    if (!match) {
      res
        .status(400)
        .json({ message: "Wrong Email and Password Combination!" });
    } else {
      const accessToken = createTokens(user);

      res.cookie("access-token", accessToken, {
        maxAge: 60 * 60 * 24 * 30 * 1000,
        httpOnly: true,
      });

      // res.json("LOGGED IN");
      res.json({
        accessToken,
        user
      })
    }
  });
});

app.get("/profile", validateToken, (req, res) => {
  res.json("profile");
});

db.sequelize.sync().then(() => {
  app.listen(3001, () => {
    console.log("SERVER RUNNING ON PORT 3001");
  });
});
